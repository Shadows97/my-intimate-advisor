import React from 'react';
import Svg, { Path } from 'react-native-svg';
import { StyleSheet } from 'react-native';


export default function FooterComponent() {
    return (
        < >
            
            <Svg height="100" width="180"  viewBox="0 0 800 90" style={styles.container} >
                <Path fill="#431562" fillOpacity="0.5"  d="M0 0v134c35 13 59 31 125 31 104 0 104-44 209-44s105 44 209 44 105-44 209-44 105 44 209 44 105-44 209-44 106 44 209 44 105-44 209-44h12V0z"></Path>
                <Path fill="#431562" fillOpacity="0.9"  d="M0 0v114c35 13 59 31 125 31 104 0 104-44 209-44s105 44 209 44 105-44 209-44 105 44 209 44 105-44 209-44 106 44 209 44 105-44 209-44h12V0z" ></Path>
            </Svg>
            
        </>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      transform: [{ rotate: '180deg' }, {scale:2.1}]
    }
  });
  
