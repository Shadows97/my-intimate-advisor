import React from 'react';
import { StyleSheet,View } from 'react-native';
import { Icon} from 'native-base';

export default function MicComponent(props) {
    return (
        <View style={styles.container}> 
            
            <Icon type="FontAwesome" name={props.isRecord? "volume-up":"microphone"} style={{fontSize: 30, color: '#431562'}} />
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        height: 80,
     width: 80,
        backgroundColor: '#FFFFFF',
        borderRadius: 80,
        alignItems: 'center',
        justifyContent: 'center',
        shadowOffset: {
            width: 200,
            height: 200
        },
        shadowOpacity: 0.1,
        shadowColor: "#E1D6FF9C"
    },
    icon: {
        fontSize: 30,
        color: '#431562'
    }
  });
  