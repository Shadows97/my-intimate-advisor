import React from 'react';
import { StyleSheet, Image, View, Text } from 'react-native';
import Footer from './FooterComponent';

export default function StartComponent() {
    return (
        <>
                <View style={styles.imageView}>
                    <Image
                        source={require('../assets/icon.png')}
                        style={styles.image}
                    />

                    <Text style={styles.homeText} >
                        Faites connaissance avec votre Conseillère intime
                    </Text>
                    

                    <Text style={styles.homeText1} >
                        Appuyez sur le bouton en bas pour parler
                    </Text>
                    
                    <View style={styles.footer}>
                        <Footer  />
                </View>
                   
                </View> 
        </>
    )
}



const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    footer: {
      position: 'absolute',
      bottom: "-150%"
      
    },
    mic: {
      position: 'absolute',
      bottom: 20
      
    },
    image: {
        width: 132,
        height: 132,
        
    },
    imageView: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 39,
        top: '20%',
        position: 'absolute',
        flex: 1,
        
    },
    homeText: {
        fontSize: 20,
        textAlign: 'center',
        fontFamily: 'Roboto'
    },
    homeText1: {
        fontSize: 12,
        textAlign: 'center',
        marginTop: 20,
        color: '#B9B9B9',
        fontFamily: 'Roboto'
    }
  });
  
