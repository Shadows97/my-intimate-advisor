import React, { Component } from 'react';
import { StyleSheet, Image, View, Text, ScrollView, FlatList } from 'react-native';
import Footer from './FooterComponent';
import Message from './MessageComponent';
import { connect} from 'react-redux';
class ChatComponent extends Component {


    constructor(props){
        super(props)
        this.state = {
            messages: this.props.messages

        }

        this.scrollView = null

    }


    componentDidUpdate() {
        this.scrollView.scrollToEnd()
    }


    render() {

        console.log(this.props.messages)

        return (
            <>

                <View style={styles.header}>
                    <Image
                        source={require('../assets/icon.png')}
                        style={styles.image}
                    />
                    <View style={styles.textHeader}>
                        <Text style={styles.homeText}>My Intimate Advisor</Text>
                        <Text style={styles.homeText1}>To talk to me just say My friend </Text>
                    </View>
                </View>


                <ScrollView style={styles.scrollView}
                            ref={ref => this.scrollView = ref}>

                    {this.props.messagesList.map((e) => {
                        return <Message message={e.text} isBot={e.isBot} key={e.id}/>
                    })}

                </ScrollView>

                {/*<FlatList*/}
                {/*    data={this.props.messagesList}*/}
                {/*    renderItem={({item})=> <Message message={item.text} isBot={item.isBot} />}*/}
                {/*    keyExtractor={(item) => item.id}*/}
                {/*    onEndReached={() => this.setState({ messages: this.props.messages})}*/}
                {/*    onEndReachedThreshold={1}*/}
                {/*    style={styles.scrollView}*/}
                {/*    extraData={this.props}*/}
                {/*    inverted={true}*/}
                {/*    */}
                {/*    */}
                {/*    />*/}


                <View style={styles.mic} >


                </View>
                <View style={styles.mic1} >


                </View>
                <View style={styles.mic2} >


                </View>

                <View style={styles.footer}>
                        <Footer  />
                </View>
            </>
        )
    }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'flex-start',
      justifyContent: 'flex-start',
    },
    footer: {
      position: 'absolute',
      bottom: "-5%"

    },
    header: {
      position: 'absolute',
        top: 50,
        left: 0,
        margin: 10,
      flexDirection: 'row'

    },
    image: {
        width: 64,
        height: 64,

    },
    scrollView: {
        height: 10,
        marginTop: 150,
        marginBottom: 120,
        width: "100%",
        padding: 10,
        zIndex: 100

    },
    homeText: {
        fontSize: 18,
        fontFamily: 'Roboto',
    },
    homeText1: {
        fontSize: 12,
        color: '#B9B9B9',
        fontFamily: 'Roboto'
    },
    textHeader: {
        paddingTop: 15
    },
    mic: {
       position: 'absolute',
        bottom: -100,
        height: 400,
        width: 400,
           backgroundColor: '#B9B9B9',
        borderRadius: 580,
        opacity: 0.1,
           zIndex: -100

    },
    mic1: {
        position: 'absolute',
         bottom: 0,
         height: 120,
         width: 120,
            backgroundColor: '#fff',
         borderRadius: 80,
        opacity: 0.4,
        zIndex: -130

    },
    mic2: {
        position: 'absolute',
         bottom: -30,
         height: 200,
         width: 200,
            backgroundColor: '#fff',
         borderRadius: 220,
        opacity: 0.5,
        zIndex: -120

     }
  });


  const mapStateToProps = (state) => {
    return {
        messages: state.messages
    }

}
export default connect(mapStateToProps)(ChatComponent)

