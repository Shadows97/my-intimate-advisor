import React from 'react';
import { StyleSheet, Image, View, Text, ScrollView, Dimensions } from 'react-native';


export default function MessageComponent(props) {


    
    return (
        <View style={[styles.container, {justifyContent: props.isBot? 'flex-start' : 'flex-end'}]}>
          {props.isBot?  <Image
                source={require('../assets/icon.png')}
                style={styles.image}
                    /> : null }
           
            
            <Text style={props.isBot ? styles.botText : styles.userText}>{props.message}</Text>
            
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',   
        flexDirection: 'row',
      margin: 10
    },
    image: {
        width: 64,
        height: 64,
        
    },
    botText: {
        fontSize: 12,
        fontFamily: 'Roboto',
        backgroundColor: '#EFEFEF',
        width: 200,
        // height: 40,
        padding: 10,
        borderTopRightRadius: 15,
        borderBottomLeftRadius: 15,
        borderBottomRightRadius: 15,
        textAlign: 'center',
        marginTop: 20
        
    },
    userText: {
        fontSize: 12,
        fontFamily: 'Roboto',
        backgroundColor: '#EFEFEF',
        width: 200,
        // height: 40,
        padding: 10,
        borderTopLeftRadius: 15,
        borderBottomLeftRadius: 15,
        borderBottomRightRadius: 15,
        textAlign: 'center',
        marginTop: 20,
        alignSelf: 'flex-end'
        
    }
  });

  