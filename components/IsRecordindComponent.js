import React from 'react';
import { StyleSheet, Image, View, Text } from 'react-native';

export default function IsRecordindComponent({msg}) {
    return (
        <View style={styles.container} >


            <Text style={{color: 'white'}}> {msg}</Text>
            <View style={styles.mic} >


            </View>
            <View style={styles.mic1} >


            </View>
            <View style={styles.mic2} >


            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#431562',
      alignItems: 'center',
        justifyContent: 'center',

    },
    footer: {
      position: 'absolute',
      bottom: "-150%"

    },
    mic: {
       position: 'absolute',
        bottom: -100,
        height: 400,
        width: 400,
           backgroundColor: '#fff',
        borderRadius: 580,
           opacity: 0.1

    },
    mic1: {
        position: 'absolute',
         bottom: 0,
         height: 120,
         width: 120,
            backgroundColor: '#fff',
         borderRadius: 80,
            opacity: 0.4

    },
    mic2: {
        position: 'absolute',
         bottom: -30,
         height: 200,
         width: 200,
            backgroundColor: '#fff',
         borderRadius: 220,
            opacity: 0.2

     }
  });

