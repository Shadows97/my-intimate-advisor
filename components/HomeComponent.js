import React, { Component } from 'react';
import { StyleSheet, Image,View,TouchableOpacity, Text } from 'react-native';
import Mic from './MicComponent';
import Start from './StartComponent';
import IsRecording from './IsRecordindComponent'
import Chat from './ChatComponent';
import { Input } from 'native-base';
import { connect } from 'react-redux';
import axios from 'axios';
import * as Utils from '../utils/utils';
import Voice, {
    SpeechRecognizedEvent,
    SpeechResultsEvent,
    SpeechErrorEvent,
} from '@react-native-community/voice';
import Tts from 'react-native-tts';


Tts.setDefaultLanguage('en-US');
Tts.setDefaultVoice('en-us-x-sfg-local');

class HomeComponent extends Component {


    constructor(props) {
        super(props);
        this.state = {
            isReady: false,
            isRecording: false,
            startChat: false,
          record: null,
          messages: [],
            inputValue: "",
            id: "",

            recognized: '',
            pitch: '',
            error: '',
            end: '',
            started: '',
            results: [],
            partialResults: [],
        };

        Voice.onSpeechStart = this.onSpeechStart;
        Voice.onSpeechRecognized = this.onSpeechRecognized;
        Voice.onSpeechEnd = this.onSpeechEnd;
        Voice.onSpeechError = this.onSpeechError;
        Voice.onSpeechResults = this.onSpeechResults;
        Voice.onSpeechPartialResults = this.onSpeechPartialResults;
        Voice.onSpeechVolumeChanged = this.onSpeechVolumeChanged;
    }





    componentWillUnmount() {
        Voice.destroy().then(Voice.removeAllListeners);
    }

    onSpeechStart = (e) => {
        console.log('onSpeechStart: ', e);
        this.setState({
            started: '√',
        });
    };

    onSpeechRecognized = (e) => {
        console.log('onSpeechRecognized: ', e);
        this.setState({
            recognized: '√',
        });
    };

    onSpeechEnd = (e) => {
        console.log('onSpeechEnd: ', e);
        this.setState({
            end: '√',
        });
    };

    onSpeechError = (e) => {
        console.log('onSpeechError: ', e);
        this.setState({
            error: JSON.stringify(e.error),
        });
    };

    onSpeechResults = (e) => {
        console.log('onSpeechResults: ', e)
        this.setState({
            results: e.value,
        }, () =>{
            this._message(this.state.results[0])
        });
    };

    onSpeechPartialResults = (e) => {
        console.log('onSpeechPartialResults: ', e);
        this.setState({
            partialResults: e.value,
        });
    };

    onSpeechVolumeChanged = (e) => {
        //console.log('onSpeechVolumeChanged: ', e);
        this.setState({
            pitch: e.value,
        });
    };

    _startRecognizing = async () => {
        this.setState({
            recognized: '',
            pitch: '',
            error: '',
            started: '',
            results: [],
            partialResults: [],
            end: '',
        });
        this.setState({ isRecording: true })

        try {
            await Voice.start('en-US');
        } catch (e) {
            console.error(e);
        }
    };

    _stopRecognizing = async () => {
        try {
            await Voice.stop();
            this.setState({ isRecording: false })
        } catch (e) {
            console.error(e);
        }
    };

    _cancelRecognizing = async () => {
        try {
            await Voice.cancel();
        } catch (e) {
            console.error(e);
        }
    };

    _destroyRecognizer = async () => {
        try {
            await Voice.destroy();
        } catch (e) {
            console.error(e);
        }
        this.setState({
            recognized: '',
            pitch: '',
            error: '',
            started: '',
            results: [],
            partialResults: [],
            end: '',
        });
    };

    _message = async (msg) => {
        try {
            this.addMessage(this.formatMessage(msg, false, Utils.range(0, 1000)))
            this.setState({messages: [...this.state.messages, this.formatMessage(msg, false, Utils.range(0, 1000))]})

            const bot = await axios.post('http://167.86.100.164:5002/bot', JSON.stringify({ text: msg, lang: "en" }), {
                headers: {
                    'Content-Type': 'application/json',
                }
            });



            console.log('There was an resp', bot.data.text);
            if (Array.isArray(bot.data.text)){
                bot.data.text.forEach(data => {
                    this.addMessage(this.formatMessage(data, true, Utils.range(0, 1000)))
                    this.setState({messages: [...this.state.messages, this.formatMessage(data, true, Utils.range(0, 1000))]})
                    Tts.speak(data, {
                        androidParams: {
                            KEY_PARAM_PAN: -1,
                            KEY_PARAM_VOLUME: 0.5,
                            KEY_PARAM_STREAM: 'STREAM_MUSIC',
                        }
                    });
                })
            } else {
                this.addMessage(this.formatMessage(bot.data.text, true, Utils.range(0, 1000)))
                this.setState({messages: [...this.state.messages, this.formatMessage(bot.data.text, true, Utils.range(0, 1000))]})
                Tts.speak(bot.data.text, {
                    androidParams: {
                        KEY_PARAM_PAN: -1,
                        KEY_PARAM_VOLUME: 0.5,
                        KEY_PARAM_STREAM: 'STREAM_MUSIC',
                    }
                });
            }


        } catch(error) {
            console.log('There was an error', error);

        }
    }


    addMessage = (message) => {
        const action = { type: "ADD_MESSAGE", value: message }
        this.props.dispatch(action)
    }

    formatMessage = (message, isBot, id) => {
        return {
            text: message,
            isBot: isBot,
            id: id
        }
    }


    render() {


        return (
            <View style={[styles.container, {backgroundColor: this.state.isRecording? '#431562' :'#fff'}]}>


            {this.state.isRecording ? <IsRecording  /> : <Chat messagesList={this.state.messages}/>}



                <View style={styles.mic} >
                    <TouchableOpacity
                        activeOpacity={1}
                        // onPressIn={this.startRecord}
                        onPressOut={() =>{this._stopRecognizing()}}
                    onLongPress={() => {this._startRecognizing()}}>
                        <Mic isRecord={this.state.isRecording}/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    footer: {
      position: 'absolute',
      bottom: "-150%"

    },
    mic: {
      position: 'absolute',
      bottom: 20

    },
    image: {
        width: 132,
        height: 132,

    },
    imageView: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 39,
        top: '20%',
        position: 'absolute',
        flex: 1,

    },
    homeText: {
        fontSize: 20,
        textAlign: 'center',
        fontFamily: 'Roboto'
    },
    homeText1: {
        fontSize: 12,
        textAlign: 'center',
        marginTop: 20,
        color: '#B9B9B9',
        fontFamily: 'Roboto'
    }
  });


const mapStateToProps = (state) => {
    return {
        messages: state.messages
    }
}
export default connect(mapStateToProps)(HomeComponent)


