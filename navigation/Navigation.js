import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from '../components/HomeComponent';





const Stack = createStackNavigator();


function AppNavigator() {
    return (
    
        <Stack.Navigator>
            <Stack.Screen
                name="Home"
                component={Home}
                options={{ headerShown: false }}/>
        </Stack.Navigator>
      
    );
  }
  
  export default AppNavigator;