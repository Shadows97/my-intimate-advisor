const initialState = {
    messages: []}

function chatMessage(state = initialState, action) {
    let nextState;
    let test
    switch (action.type) {
        case 'ADD_MESSAGE':
            //console.log(action)
            test = state.messages
            console.log(test)
            test.push(action.value)
            console.log(test)
            nextState = {
                ...state,
                messages: test
            }
            
            return nextState || state;
       
        default:
            return state;
    }
    
}


export default chatMessage;