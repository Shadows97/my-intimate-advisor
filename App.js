/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { StyleSheet, Text, View, TouchableHighlight, Image } from 'react-native';
import Footer from './components/FooterComponent';
import Mic from './components/MicComponent';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import AppNavigator from './navigation/Navigation';
import Home from './components/HomeComponent';
import {Provider} from 'react-redux';
import Store from './store/configureStore';

export default function App() {
  return (

      <Provider store={Store}>
        {/* <NavigationContainer>
       <AppNavigator/>

     </NavigationContainer> */}
        <Home/>
      </Provider>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  footer: {
    position: 'absolute',
    bottom: -30

  },
  mic: {
    position: 'absolute',
    bottom: 20

  }
});
